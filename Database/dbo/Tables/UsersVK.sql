﻿CREATE TABLE [dbo].[UsersVK] (
    [Id]      INT            NOT NULL,
    [Email]   NVARCHAR (100) NOT NULL,
    [Name]    NVARCHAR (100) NULL,
    [Surname] NVARCHAR (100) NULL,
    [VkPage]  NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


