﻿CREATE TABLE [dbo].[Notes] (
    [note_guid] UNIQUEIDENTIFIER NOT NULL,
    [note_id]   NVARCHAR (70)    NOT NULL,
    [user_id]   NVARCHAR (100)   NOT NULL,
    [note_head] NVARCHAR (300)   NULL,
    [note_body] NVARCHAR (4000)  NOT NULL,
    [isHidden]  BIT              CONSTRAINT [DF_Notes_isHidden] DEFAULT ((0)) NOT NULL,
    [isRemoved] BIT NOT NULL DEFAULT ((0)), 
    [isWrapped] BIT NOT NULL DEFAULT ((0)), 
    [dttm_created] DATETIME NULL DEFAULT CURRENT_TIMESTAMP, 
    [dttm_updated] DATETIME NULL, 
    CONSTRAINT [PK_Notes] PRIMARY KEY CLUSTERED ([note_guid] ASC)
);

