using System.Collections.Generic;
using Microsoft.Owin.Security.Provider;

namespace Microsoft.Owin.Security.VK
{
    public class VkReturnEndpointContext : ReturnEndpointContext
    {
        public VkReturnEndpointContext(
            IOwinContext context,
            AuthenticationTicket ticket,
            IDictionary<string, string> errorDetails)
            : base(context, ticket, errorDetails)
        {
        }
    }
}
