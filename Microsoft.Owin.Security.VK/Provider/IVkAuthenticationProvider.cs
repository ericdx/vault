using System.Threading.Tasks;

namespace Microsoft.Owin.Security.VK
{
    public interface IVkAuthenticationProvider
    {
        Task Authenticated(VkAuthenticatedContext context);
        Task ReturnEndpoint(VkReturnEndpointContext context);
    }
}
