﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data;
using Logic;
using vault.Models;

namespace vault.Controllers
{
    public class NoteController : Controller
    {
        VaultEntities ctx = new VaultEntities();
        NotesService ns = new NotesService();
        // GET: Note
        public JsonResult Save(string noteId,string noteHead,string noteBody)
        {            
            NoteSaveResponse resp = new NoteSaveResponse();
            var newId = Guid.NewGuid();
            var editedNote = new Notes
            {
                note_guid = newId,
                user_id = User.Identity.Name,
                note_id = "",
                note_head = noteHead,
                note_body = noteBody
            };
            try {
                Guid guid;
                Guid.TryParse(noteId,out guid);
                Notes existNote;
                if (guid == Guid.Empty)
                {                    
                        ctx.Notes.Add(editedNote);                    
                    resp.status = SaveStatus.NewId;
                    resp.message = newId.ToString();
                }
                else
                {
                    existNote = ctx.Notes.SingleOrDefault(n => n.note_guid == guid && n.user_id == User.Identity.Name);
                    if (existNote == null)
                        ctx.Notes.Add(editedNote);
                    else {
                        existNote.note_head = editedNote.note_head;
                        existNote.note_body = editedNote.note_body;
                        existNote.dttm_updated = DateTime.Now;
                    }
                    resp.status = SaveStatus.Succeed;
                }
                ctx.SaveChanges();
            } catch (Exception ex)
            {
                resp.status = SaveStatus.Error;
                resp.message = ex.Message;
            }
            return Json(resp,JsonRequestBehavior.AllowGet);
        }

        public string Remove(string note_guid)
        {
            return Delete(note_guid, true);
        }

        public string Delete(string note_guid,bool permanent = false)
        {
            try {
                var guid = Guid.Parse(note_guid);
                var existNote = ctx.Notes.SingleOrDefault(n => n.note_guid == guid && n.user_id == User.Identity.Name);
                if (existNote == null)
                    return "";
                if ( ! permanent )
                    existNote.isHidden = true;
                else
                {
                    existNote.isRemoved = true;
                }
                ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                return "Ошибка " + ex.Message;
            }
            return "";
        }

        public JsonResult ajaxNoteByGuid(string guid)
        {
            NoteModel mdl; 
            try {
                var noteGuid = Guid.Parse(guid);
                var note = ctx.Notes.SingleOrDefault(n => n.note_guid == noteGuid);
                if (note != null)
                {
                    mdl = new NoteModel { guid = note.note_guid, head = note.note_head,body = note.note_body  };
                } else
                    mdl = new NoteModel { head = "Error", body = "Not found :(" };
            }
            catch (Exception ex)
            {
                mdl = new NoteModel { head = "Error", body = ex.Message };
            }  
            return Json(mdl,JsonRequestBehavior.AllowGet);                 
        }
        public void ajaxOperateNote(string note_guid,string operation)
        {
            var guid = Guid.Parse(note_guid);
            var note = ctx.Notes.SingleOrDefault(n => n.note_guid == guid && n.user_id == User.Identity.Name);
            if (note != null)
            {
                var op = int.Parse(operation);
                switch (op)
                {
                    case (int)Operations.Recover:
                        note.isHidden = false;
                        break;
                    case (int)Operations.Wrap:
                        note.isWrapped = true;
                        break;
                    case (int)Operations.Unwrap:
                        note.isWrapped = false;
                        break;
                }                
                ctx.SaveChanges();
            }
            
        }       

    }
}