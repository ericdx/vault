﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Logic;
using Data;

namespace vault.Controllers
{
    public class HomeController : Controller
    {
        NotesService ns = new NotesService();
        VaultEntities ctx = new VaultEntities();
        public ActionResult Index()
        {
            List<NoteModel> notes = new List<NoteModel>();
            if (User.Identity.IsAuthenticated)
            {
                notes = ns.loadUserNotes(User.Identity.Name);

            }
            
            return View(notes);
        }

        public ActionResult About()
        {        

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }        

        public ActionResult DayPicture(int picNum)
        {
            if (picNum > DateTime.Now.DayOfYear)
                picNum = DateTime.Now.DayOfYear;
            ViewBag.keywords = "картинка дня";
            return View(picNum);
        }

        public ActionResult History()
        {
            var notes = ctx.Notes.Where(n => n.isHidden && !n.isRemoved && n.user_id == User.Identity.Name)
                .Select(note => new NoteModel { guid = note.note_guid,head = note.note_head }).ToList();
            
            return View(notes);
        }
        public ActionResult SetCulture(string culture)
        {
            Session["culture"] = culture;
            return RedirectToAction("Index", "Home");
        }
    }
}