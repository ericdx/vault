﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(vault.Startup))]
namespace vault
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
