﻿
VK.init({
    apiId: 5436195
});
function authInfo(response) {
    if (response.session) {
        var usr = response.session.user;
        var name = usr.first_name;
        var surname = usr.last_name;
        var vkPage = usr.href;
        window.location = '/Account/EnterVK?id=' + response.session.mid +
            '&name=' + name + '&surname=' + surname + '&vkPage=' + vkPage;
    }
}