﻿
$(function () {
    if (!isAuthenticated && !navigator.cookieEnabled) {
        $('#notificationArea').append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>   <span aria-hidden='true'>x</span>  </button>Похоже, ваш браузер не поддерживает cookie, поэтому функционал сайта не доступен. Пожалуйста, включите cookie в настройках вашего браузера")
            .show();
    } 
    //load cookies after db notes
    var cooks = Cookies.get();
    Object.keys(cooks).forEach(function (key, index) {
        //filter notes only
        if (key.lastIndexOf(cookiePrefix, 0) === 0) {
            var parsed = Cookies.getJSON(key);
            var cookNote = aNote.replace(headTempl, parsed.head);
            cookNote = cookNote.replace(bodyTempl, parsed.body);
            $('#noteField').append(cookNote);
            //set name without prefix
            var new_name = key.substring(cookiePrefix.length);
            $('#note_new').attr('id', new_name);
            if (isAuthenticated) {
                var indic = $('.saving');
                indic.show();
                //move cookie to db when registered
                saveNoteDb(new_name, parsed.head, parsed.body, indic);
                Cookies.remove(key);
            }
        }
    });
    //add new
    $('#noteField').append(newNote);
    $('#note_new .manage').hide();
    manipulateDOM(); //note field is rendered, manipulate DOM here
    $('li').click(wrappedClick);
}); //$(function () {

var newNote = prepareNewNote(aNote);

var typingTimer;                //timer identifier
var doneTypingInterval = 2000;  //time in ms, 5 second for example
var $input;

var editNoteId;

const wasNew = 'note_wasNew';

const cookiePrefix = '_note_';

function saveNotes() {
    var allNotes = $("div[name=note]");
    $.each(allNotes, function (ind, note) {
        var body = $(this).find('textarea').val();

        var head = $(this).find('input').val();

        if (body != '' && $(this).attr('data-edited') != undefined) {
            $(this).removeAttr('data-edited');
            var indic = $(this).find('.saving');
            indic.show();
            var note_guid = $(this).attr('id'); //can be wasNew, not guid if cookie

            var cookNote = {
                head: head,
                body: body
            }

            if (!isAuthenticated) {
                //save cookie
                Cookies.set(cookiePrefix + note_guid, cookNote, { expires: 500, path: '' });
                indic.hide();
            }
            else {
                saveNoteDb(note_guid, head, body, indic);
            }
        }
    });
}

function saveNoteDb(note_guid, head, body,indic) {
    $.getJSON('/Note/Save', {
        noteId: note_guid,
        noteHead: head,
        noteBody: body
    }, function (ret) {
        if (ret.status == '1')
            $('#errMsg').html(ret.message);
        else {
            indic.hide();
            if (ret.status == '2') //new guid assigned in db, map on client
                $('#' + note_guid).attr('id', ret.message);
        }
    });
}

function registerRecover(id) {
    var note = $('#' + id);
    note.resizable();
    var recover = note.find('.recoverBtn');
    recover.attr('title', 'Развернуть')
    recover.click(function () {
        note.parent('.alert-success').hide();
        $.get('/Note/ajaxOperateNote', { note_guid: id, operation: '2' });
        //show note in unwrapped region
        note.addClass('col-md-4');
        note.find('input,textarea').prop('disabled',false);
        recover.remove();
        $('#noteField').append(note.prop("outerHTML"));
        listenInputs(note);
        registerRemove(id);
    });
}

//user is "finished typing," do something
function doneTyping() {
    addNewNote();
    saveNotes();
}

function prepareNewNote(noteTempl) {
    var newNote = noteTempl.replace(headTempl, '');
    newNote = newNote.replace(bodyTempl, '');
    return newNote;
}



function setHandlers(el) {
    listenInputs(el);
    el.find('.deleteBtn').click(function () {
        var note = $(this).parents('div .note'); //note id
        //kill from cookies
        var note_guid = note.attr('id');
        if (note_guid == undefined)
            return;
        var cookieKey = cookiePrefix + note_guid;
        var cook = Cookies.get(cookieKey);
        if (cook == undefined && isAuthenticated)
            $.get('/Note/Delete', { note_guid: note_guid }, function (ret) {
                if (ret != '')
                    $('#errMsg').html(ret);
            });
        else Cookies.remove(cookieKey);
        note.remove();
    });
    el.find('.wrapBtn').click(function () {
        var note = $(this).parents('div[name=note]');
        if (isAuthenticated) {
            var note_guid = note.attr('id');
            note.removeAttr('id');
            var noteName = note.find('input').val();
            noteName = (noteName == '') ? 'Без имени' : noteName;
            $('#wrappedField > ul').append("<li class='pointer' id='" + note_guid + "'><a >" + noteName + "</a></li>");
            $('#' + note_guid).click(wrappedClick);
            $.get('/Note/ajaxOperateNote', { note_guid: note_guid, operation: '1' });
            note.remove();

        } else {
            note.append("<div class='alert alert-info alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Пожалуйста, авторизуйтесь, чтобы сворачивать заметки</div > ");
        }
    });
}

function listenInputs(el) {
    $input = el.find('textarea,input');
    $input.unbind();
    //on keyup, start the countdown
    $input.bind('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });
    //on keydown, clear the countdown
    $input.bind('keydown', function () {
        clearTimeout(typingTimer);
        $(this).parents('div[name=note]').attr('data-edited', '');
    });
}

function manipulateDOM() {
    allNotes = $("div[name=note]");
    if (/Mobi/.test(navigator.userAgent) == false) {
        // not mobile!
        allNotes.find('textarea').attr('cols', 35);
        $('#note_new input').focus();
    }
    allNotes.draggable().resizable();
    setHandlers(allNotes);
}

function addNewNote() {
    var allFilled = true;
    var allNotes = $("div[name=note]");
    $.each(allNotes, function (ind, note) {
        if ($(this).find('textarea').val() == '') {
            allFilled = false;
        } else $(this).find('.manage').show();
    });
    if (allFilled) {
        //rename all cookie 'wasNew'
        $.each(allNotes, function (ind, note) {
            var currentId = $(this).attr('id');
            if (currentId.lastIndexOf(wasNew, 0) === 0) {
                var wasIdNum = currentId.substring(wasNew.length);
                if (wasIdNum == '')
                    wasIdNum = '0';
                var num = Number.parseInt(wasIdNum, 10) + 1;
                $(this).attr('id', wasNew + num);
            }
        });
        //
        $('#note_new').attr('id', wasNew);
        $('#noteField').append(newNote);
        $('#note_new .manage').hide();
        $('#note_new').draggable().resizable();
        setHandlers($('#note_new'));
    }
}
