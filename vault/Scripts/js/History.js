﻿

function registerRecover(id) {
    var note = $('#' + id);
    note.resizable();    
    var recover = note.find('.recoverBtn');
    recover.attr('title','Восстановить')
    recover.click(function () {
        note.parent('.alert-success').hide();
        $.get('/Note/ajaxOperateNote', { note_guid: id , operation: '0' });
    });
}
