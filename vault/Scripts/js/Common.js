﻿
function registerRemove(id) {
    var note = $('#' + id);
    var remove = note.find('.removeBtn');
    remove.click(function () {
        $.get('/Note/Remove', { note_guid: id }, function (ret) {
            note.parents('.alert-success').hide();
            note.hide();
        });
    });
}