﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vault.Models
{
    public enum SaveStatus
    {
        Succeed,
        Error,
        NewId
    }
    public class NoteSaveResponse
    {
        public SaveStatus status;
        public string message;
    }
}