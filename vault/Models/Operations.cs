﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vault.Models
{
    public enum Operations
    {
        Recover,
        Wrap,
        Unwrap
    }
}