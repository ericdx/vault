﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Logic
{
    public class NotesService
    {
        VaultEntities ctx = new VaultEntities();
        public List<NoteModel> loadUserNotes(string user)
        {
           var notes = ctx.Notes.Where(n => n.user_id == user 
                && ! n.isHidden && ! n.isRemoved && ! n.isWrapped ).
                Select(n => new NoteModel
                {
                    guid = n.note_guid,
                    id = n.note_id,
                    head = n.note_head,
                    body = n.note_body,
                    isWrapped = n.isWrapped,
                }).ToList();

            //only head & guid for wrapped
            var wrapped = ctx.Notes.Where(n => n.user_id == user 
                && ! n.isHidden && ! n.isRemoved && n.isWrapped ).
                Select(n => new NoteModel {
                    guid = n.note_guid, head = n.note_head, isWrapped = n.isWrapped }).ToList();

            notes.AddRange(wrapped);

            return notes;
        }
    }
}
