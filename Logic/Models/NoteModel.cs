﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Data;

namespace Logic
{
    public class NoteModel
    {
        public NoteModel() { }       
        
        public Guid guid; //common
        public string id; //user-specific
        public string head;
        public string body;
        public bool isWrapped;
    }
}